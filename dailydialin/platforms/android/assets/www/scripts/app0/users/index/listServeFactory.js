﻿angular.module("ModuleHotLine").factory('ListServ', ['$rootScope', '$timeout', '$window', 'userViewModelHttp', 'timeZoneHttp', "hotlineFormattingFactory", "appConfigs", "appUser",
    function ($rootScope, $timeout, $window, userViewModelHttp, timeZoneHttp, hotlineFormattingFactory, appConfigs, appUser) {
        var userGridDS = undefined;
        var gridColumns = undefined;
        var kMobileApp = kendo.mobile.application;
        $rootScope.hotlineFormattingFactory = hotlineFormattingFactory;
        var getNewRecurrenceValue = function (dataItem) {
            var recurrenceValue = 'FREQ=WEEKLY;BYDAY=';
            if (dataItem.RVMO)
                recurrenceValue += 'MO,';
            if (dataItem.RVTU)
                recurrenceValue += 'TU,';
            if (dataItem.RVWE)
                recurrenceValue += 'WE,';
            if (dataItem.RVTH)
                recurrenceValue += 'TH,';
            if (dataItem.RVFR)
                recurrenceValue += 'FR,';
            if (dataItem.RVSA)
                recurrenceValue += 'SA,';
            if (dataItem.RVSU)
                recurrenceValue += 'SU';
            recurrenceValue += ';WKST=SU';
            return recurrenceValue;
        };
        var getUserGridDS = function () {
            userGridDS = new kendo.data.DataSource({
                type: "odata-v4",
                transport: {
                    read: function (options) {
                        var data = {};
                        $.each(options.data, function (key, value) {
                            if (value != undefined)
                                data[key] = value;
                        });
                        options.data = data;
                        var oDataOptions = kendo.data.transports.odata.parameterMap(options.data, "read");
                        delete oDataOptions.$inlinecount;
                        oDataOptions.$count = true;

                        userViewModelHttp.getAll(oDataOptions)
                                                 .success(function (data) {
                                                     $.each(data.value, function (idx, item) {
                                                         item.dtCallTime = hotlineFormattingFactory.getDtCallTime(item.CallTime);
                                                         item.TimeToCall = hotlineFormattingFactory.getFormattedCallTime(item.CallTime);
                                                     });
                                                     options.success(data);

                                                 });
                    },
                    update: function (options) {
                        var userViewModel = options.data.models[0];


                        var userViewModelsParam = {
                            FirstName: userViewModel.FirstName,
                            LastName: userViewModel.LastName,
                            EmailAddress: userViewModel.EmailAddress,
                            RemainingFreeDays: userViewModel.RemainingFreeDays,
                            TimeZoneName: userViewModel.TimeZoneName,

                            HotLinePhoneNumber: userViewModel.HotLinePhoneNumber,
                            HotLineAdditionalInput: userViewModel.formattedAdditionalInput.replace(/ /g, "").replace(/sec/g, ""),
                            RecurrenceValue: getNewRecurrenceValue(userViewModel),
                            EmailNotify: userViewModel.EmailNotify,
                        };

                        if (userViewModel.CallTime)
                            userViewModelsParam.CallTime = new Date(userViewModel.CallTime.getTime() - userViewModel.CallTime.getTimezoneOffset() * 60 * 1000);

                        if (appUser.EmailAddress == appConfigs.adminEmailAddress) {
                            userViewModelsParam.IsAdmin = userViewModel.IsAdmin;
                        }

                        userViewModelsParam.MobileNumbers = [];
                        angular.forEach(userViewModel.MobileNumbersForm, function (item, index) {
                            if (item.value != null)
                                userViewModelsParam.MobileNumbers.push('' + item.value);
                        });

                        kMobileApp.showLoading();
                        userViewModelHttp.update(userViewModel.Id, userViewModelsParam)
                                         .success(function (data) {
                                             kMobileApp.hideLoading();
                                             data.dtCallTime = hotlineFormattingFactory.getDtCallTime(data.CallTime);
                                             data.TimeToCall = hotlineFormattingFactory.getFormattedCallTime(data.CallTime);
                                             delete data["@odata.context"];
                                             options.success({ value: data });
                                         });
                    },
                    destroy: function (options) {
                        var userViewModel = options.data.models[0];
                        kMobileApp.showLoading();
                        userViewModelHttp.delete(userViewModel.Id)
                                         .success(function (data) {
                                             kMobileApp.hideLoading();
                                             options.success(data);
                                         });
                    },
                    parameterMap: function (options) {
                    }
                },
                schema: {
                    total: function (data) { return data['@odata.count']; },
                    data: "value",
                    model: {
                        id: "Id",
                        fields: {
                            Id: { type: "string" },
                            FirstName: { type: "string" },
                            LastName: { type: "string" },
                            TimeZoneUtcOffset: { type: "string" },
                            TimeZoneName: { type: "string" },
                            TimeZoneId: { type: "string" },
                            EmailAddress: { type: "string" },
                            RemainingFreeDays: { type: "number" },
                            EmailNotify: { type: "bool" },
                            HotLinePhoneNumber: { type: "string" },
                            HotLineAdditionalInput: { type: "string", editable: false },
                            CallTime: {
                                type: "date",
                            },
                            dtCallTime: { type: "date" },
                            TimeToCall: { type: "string" },
                            RecurrenceValue: { type: "string", editable: false },
                            MobileNumbers: { MobileNumber: { type: "string", parse: function (e) { return e.join(","); } } },
                            CreatedDate: { type: "date", },
                            PromoCocde: { type: "string" },
                            PromoCodeUsed: { type: "string" },
                            AccountType: { type: "string" },
                            IsAdmin: { type: "bool" },
                        }
                    }
                },
                sort: { field: "CreatedDate", dir: "desc" },
                pageSize: 500,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                change: function (e) {
                },
                batch: true,
                requestStart: function () {
                },
            });
            return userGridDS;
        };

        var getUserGridColumns = function () {
            gridColumns = [
                            { field: "Id", title: "ID", width: "50px", hidden: true, },
                             {
                                 command: ["edit", {
                                     template: '<button class="k-button k-button-icontext k-grid-delete" href="\\#" ng-disabled="dataItem.EmailAddress==appConfigs.adminEmailAddress"><span class="k-icon k-delete"></span>Delete</button>'
                                 },
                                 {
                                     text: "Login", click: function (e) {
                                         var dataItem = $("#div-leads-users").data("kendoGrid").dataItem($(e.currentTarget).parents("tr"));
                                         $window.open(appConfigs.hotLineWebUrl + '/M/Index/' + dataItem.Id, '_blank');
                                     }
                                 }], width: "180px"
                             },
                            {
                                field: "FirstName", title: "First", width: "120px",
                            },
                            {
                                field: "LastName", title: "Last", width: "120px",
                            },
                            {
                                field: "EmailAddress", title: "Email", width: "230px",
                            },
                            {
                                field: "RemainingFreeDays", title: "Remaining Days", width: "180px",
                            },
                            {
                                field: "HotLinePhoneNumber", title: "Phone Number", width: "170px",
                            },
                            {
                                field: "HotLineAdditionalInput", title: "Additional Input", width: "180px",
                            },
                            {
                                field: "TimeZoneName", title: "Time Zone", width: "150px",
                            },
                            {
                                field: "RecurrenceValue", title: "Recurrence", width: "180px", template: "{{hotlineFormattingFactory.getWeekDaysString(dataItem.RecurrenceValue)}}"
                            },
                            {
                                field: "dtCallTime", title: "Call Time", width: "130px",
                                template: "#:kendo.toString(dtCallTime, 'hh:mm tt')#",

                            },
                             {
                                 field: "MobileNumbers", title: "Mobile Numbers", width: "180px",
                                 template: '#: MobileNumbers ? MobileNumbers.join(",") : "" #',

                             },
                              {
                                  field: "CreatedDate", title: "Created Date", width: "170px", format: "{0:yyyy-MM-dd hh:mm tt}",
                              },
                               {
                                   field: "PromoCode", title: "Personal Code", width: "150px",
                               },
                               {
                                   field: "PromoCodeUsed", title: "Code Used", width: "150px",
                               },
                               {
                                   field: "AccountType", title: "Account Type", width: "160px",
                               },
                               {
                                   field: "IsAdmin", title: "Admin", width: "160px",
                               },
            ];
            return gridColumns;
        };

        return {
            userGridColumns: function () { return getUserGridColumns(); },
            userGridDS: function (leadId, startTime, endTime) { return getUserGridDS(leadId, startTime, endTime); },
        };
    }]);