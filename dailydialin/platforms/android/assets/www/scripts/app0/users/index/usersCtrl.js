﻿angular.module("ModuleHotLine").controller("UsersCtrl", ["$rootScope", "$scope", "ListServ", "$timeout", "$window", "appConfigs","appUser",
    function ($rootScope, $scope, ListServ, $timeout, $window, appConfigs, appUser) {

        $scope.gridColumns = ListServ.userGridColumns();
        $scope.gridDS = ListServ.userGridDS(78, '', '');
        $scope.appConfigs = appConfigs;
        $scope.appUser = appUser;

        var windowResized = function () {
            //var gridElement = $("#div-leads-users");
            //var dataArea = gridElement.find(".k-grid-content");
            //var newHeight = $($window).height() - 170;
            //var diff = gridElement.innerHeight() - dataArea.innerHeight();
            //gridElement.height(newHeight);
            //dataArea.height(newHeight - diff);
        };

        windowResized();

        angular.element($window).bind('resize', windowResized);

        $scope.handleChange = function (selected, dataItem, data) {
        };

        $scope.init = function (options) {
            console.log(options);
            $rootScope.model = options;
        };


        $scope.getFilteredData = function (dataSource, filters) {
            var allData = dataSource.data();
            var query = new kendo.data.Query(allData);
            var data = query.filter(filters).data;
            return data;
        };

        $scope.gridOptions = {
            sortable: true,
            pageable: true,
            dataSource: $scope.gridDS,
            columns: $scope.gridColumns,
            //toolbar: kendo.template($("#toolbar-template").html()),
            scrollable: true,
            resizable: true,
            filterable: true,
            groupable: true,
            columnMenu: true,
            editable: {
                mode: "popup",
                template: $("#gridEditUsersTemplate").html(),
                confirmation: true,
            },
            mobile: "phone",
            //selectable: "row",			
            edit: function (e) {
                //debugger;
            },
            saveChanges: function (e) {
            },
            remove: function (e) {
            },
            pageable: {
                input: true,
                numeric: false
            },
            dataBinding: function (e) {
            },
            detailInit: detailInit,
            dataBound: function () {
                //this.expandRow(this.tbody.find("tr.k-master-row").first());
            }
        };

        function detailInit(e) {
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: {
                    data: e.data.NewestRefferedUsers,
                    schema: {
                        model: {
                            Username: { type: "string", editable: false },
                            PaidAmount: { type: "integer", editable: false },
                            RemainingDays: { type: "integer", editable: false },
                        }
                    }
                },
                scrollable: true,
                resizable: true,
                filterable: true,
                groupable: true,
                sortable: true,
                columns: [
                    { field: "Username", title: "Username", width: "60px" },
                    { field: "PaidAmount", title: "Paid Amount", width: "30px" },
                    { field: "RemainingDays", title: "Remaining Days", width: "30px" },
                ]
            });
        }
    }]);


