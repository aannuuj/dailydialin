﻿angular.module("ModuleHotLine").factory("accountHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var accountFactory = {};
    var baseUrl = appConfigs.hotLineWebApiUrl;

    accountFactory.login = function (username, password, rememberMe) {
        return $http({
            method: 'POST',
            url: baseUrl + "/token",
            data: $.param({
                grant_type: 'password',
                username: username,
                password: password
            }),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' } 
        });
    };

    accountFactory.register = function (model) {
        return $http({
            method: 'POST',
            url: baseUrl + "/api/Account/Register",
            data: $.param(model),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        });
    };

    accountFactory.registerExternal = function (model) {
        return $http.get(baseUrl + '/api/Account/RegisterExternal', model);
    };

    return accountFactory;
}]);