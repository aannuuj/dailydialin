﻿angular.module("ModuleHotLine").factory("plivoHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var plivoFactory = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata";

    plivoFactory.makeTestCall = function (user_id, to, send_digits, notif_numbers, ring_timeout, notify_failure) {
        return $http.post(baseUrl + "/MakeTestCall", { user_id: user_id, to: to, send_digits: send_digits, notif_numbers: notif_numbers, ring_timeout: ring_timeout + "", notify_failure: notify_failure });
    };
    plivoFactory.getTranscription = function (callUuid) {
        return $http.post(baseUrl + "/GetTranscription", { callUuid: callUuid });
    };
    return plivoFactory;
}]);