﻿angular.module("ModuleHotLine").factory("userViewModelHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var userViewModelFactory = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata/UserViewModels";

    userViewModelFactory.getAll = function (queryStringObject) {
        return $http.get(baseUrl, {params: queryStringObject});
    };
    
    userViewModelFactory.getCallLogViewModels = function (userId, queryStringObject, content) {
        var url =baseUrl + "('" + userId + "')/CallLogViewModels";
        return $http.get(url,{params: queryStringObject});
    };
    userViewModelFactory.update = function (userId, userViewModel) {
        return $http.patch(baseUrl + "('"+ userId  + "')", userViewModel);
    };

    userViewModelFactory.delete = function (userId) {
        return $http.delete(baseUrl + "('" + userId + "')");
    };

    userViewModelFactory.changePassword = function (userId, oldPassword, newPassowrd) {
        return $http.post(baseUrl + "('" + userId + "')/HotLineModels.ViewModels.ChangePassword", { oldPassword: oldPassword, newPassword: newPassowrd });
    };
    userViewModelFactory.updateProfile = function (userId, userViewModel) {
        return $http.post(baseUrl + "('" + userId + "')/HotLineModels.ViewModels.UpdateProfile", { userViewModel: userViewModel });
    };
    userViewModelFactory.updateHotlineSettings = function (userId, userViewModel) {
        return $http.post(baseUrl + "('" + userId + "')/HotLineModels.ViewModels.UpdateHotlineSettings", { userViewModel: userViewModel });
    };

    userViewModelFactory.getAccountType = function(userEmail){
        return $http.post(baseUrl + "/HotLineModels.ViewModels.GetAccountType", { userEmail: userEmail});
    };
    return userViewModelFactory;
}]);