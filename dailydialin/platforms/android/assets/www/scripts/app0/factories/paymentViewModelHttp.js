﻿angular.module("ModuleHotLine").factory("paymentViewModelHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var paymentViewModelHttp = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata/PaymentViewModels";
   
    paymentViewModelHttp.makePayment = function (paymentViewModel) {
        return $http.post(baseUrl, paymentViewModel);
    };

    return paymentViewModelHttp;
}]);