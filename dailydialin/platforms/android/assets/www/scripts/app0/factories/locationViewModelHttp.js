﻿angular.module("ModuleHotLine").factory("locationViewModelHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var locationViewModelFactory = {};
    var baseUrl = appConfigs.trueVoiceWebApiUrl + "/odata/Leads";

    locationViewModelFactory.getLocations = function (currentTime, latitude, longitude) {
        return $http.post(baseUrl + '/SearchLeadsForOpenTime', { campaignID:  Number(appConfigs.locationsCampaignID), timeToCheck: currentTime, latitude: latitude, longitude: longitude },
        { params: {$top: 20, $orderby: 'DistanceInKm'}});
    };

    return locationViewModelFactory;
}]);