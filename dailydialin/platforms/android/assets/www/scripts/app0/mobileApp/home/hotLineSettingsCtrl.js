﻿angular.module("ModuleHotLine").controller("hotLineSettingsCtrl", ["$scope", "$sce", "$timeout", "$compile", "$window", "appUser", "userViewModelHttp", "plivoHttp",
    function ($scope, $sce, $timeout, $compile, $window, appUser, userViewModelHttp, plivoHttp) {
        var kMobileApp = kendo.mobile.application;

        $scope.onShow = function () {
            $scope.ktpTimeToCall.min(new Date(1970, 01, 01, 05, 00, 00));
            $scope.ktpTimeToCall.max(new Date(1970, 01, 01, 21, 00, 00));

            $scope.PhoneNumber = appUser.HotLinePhoneNumber.replace(/[\-() ]/g, "") == "" ? "" : Number(appUser.HotLinePhoneNumber.replace(/[\-() ]/g, ""));
            //$scope.AdditionalInput = appUser.HotLineAdditionalInput;
            $scope.dtTimeToCall = new Date(new Date(appUser.CallTime).getTime() + new Date(appUser.CallTime).getTimezoneOffset() * 60 * 1000);
            $scope.CallTimeSet = appUser.CallTime != null && appUser.CallTime != "";
            $scope.TimeToCall = kendo.toString($scope.dtTimeToCall, "hh:mm tt");
            $scope.ALL = null;
            $scope.MO = $scope.checkWeekDay('MO');
            $scope.TU = $scope.checkWeekDay('TU');
            $scope.WE = $scope.checkWeekDay('WE');
            $scope.TH = $scope.checkWeekDay('TH');
            $scope.FR = $scope.checkWeekDay('FR');
            $scope.SA = $scope.checkWeekDay('SA');
            $scope.SU = $scope.checkWeekDay('SU');
            $scope.EmailNotify = appUser.EmailNotify;
            if ($scope.MO
                && $scope.TU
                && $scope.WE
                && $scope.TH
                && $scope.FR
                && $scope.SA
                && $scope.SU) {
                $scope.ALL = true;
            }
            //$scope.CallLimit = appUser.CallLimit;
            $scope.MobileNumbers = [];
            angular.forEach(appUser.MobileNumbers, function (item, index) {
                $scope.mobileNumberAdded(Number(item.replace(/[\-() ]/g, "")));
            });
            if ($scope.MobileNumbers.length == 0) {
                $scope.mobileNumberAdded(null);
            }

            $scope.InputPairs = [];
            $scope.discardedInputPairs = [];
            angular.forEach(appUser.HotLineAdditionalInput.split(';'), function (item, index) {
                var values = item.split(',');
                var numbers = values[0];
                var wait = values[1];
                if (numbers == null) {
                    numbers = '';
                }
                if (wait == null) {
                    wait = '';
                }
                if (numbers != '' || wait != '')
                    $scope.inputsPairsAdded({ Numbers: Number(numbers), Wait: wait });
            });
            if ($scope.InputPairs.length == 0) {
                $scope.inputsPairsAdded({ Numbers: null, Wait: null });
            }
        };

        $scope.kmtbNumbersOpts = {
            mask: "dddddddddddddddddddddddddddddddddddddddd",
            promptChar: " ", //specify prompt char as empty char
            rules: {
                "d": /[*#0-9]/,
            }
        };

        $scope.numberKeyDown = function (event) {
            if (event.keyCode == 189 ||
               event.keyCode == 107 ||
                event.keyCode == 109 ||
                event.keyCode == 190)
                event.preventDefault();
        };

        $scope.timeToCallChanged = function () {
            $scope.CallTimeSet = true;
        };

        $scope.getFormattedDigits = function () {
            var additionalInput = 'wwwwww';
            angular.forEach($scope.InputPairs, function (item, index) {
                if (item.Numbers != '') {
                    additionalInput += item.Numbers;
                }
                if (item.Wait != '') {
                    for (var i = 0; i < Number(item.Wait) ; i++) {
                        additionalInput += 'ww';
                    }
                }
            });
            return additionalInput;
        };

        $scope.mobileNumberAdded = function (value) {
            $scope.MobileNumbers.push({ value: value });
        };

        $scope.mobileNumberRemoved = function (index) {
            if ($scope.MobileNumbers.length == 1)
                return;
            $scope.MobileNumbers.splice(index, 1);
        };

        $scope.inputPairsWaitChanged = function (wait, oldWait, index) {
            if (oldWait == 0 && wait != 0) {
                $scope.bringbackInputPairs();
            }

            if (wait != 0) {
                $scope.inputsPairsAdded({ Numbers: null, Wait: '' });
            }
            else if (wait == 0) {
                $scope.discardInputPairs(index);
            }
            else {
                //Will do something here in future...
            }
        };

        $scope.inputsPairsAdded = function (value) {
            if ($scope.InputPairs.length > 0 &&
                ($scope.InputPairs[$scope.InputPairs.length - 1].Wait == '' ||
                 $scope.InputPairs[$scope.InputPairs.length - 1].Wait == 0))
                return;
            $scope.InputPairs.push(value);
        };

        $scope.inputsPairsRemoved = function (index) {
            if ($scope.InputPairs.length == 1)
                return;
            $scope.InputPairs.splice(index, 1);
        };

        $scope.discardInputPairs = function (index) {
            if ($scope.InputPairs.length == 1)
                return;
            $scope.discardedInputPairs = $scope.InputPairs.splice(index + 1);
        };

        $scope.bringbackInputPairs = function () {
            $.merge($scope.InputPairs, $scope.discardedInputPairs);
            $scope.discardedInputPairs = [];
        };

        $scope.checkWeekDay = function (weekDay) {
            if (appUser.RecurrenceValue == '' ||
                appUser.RecurrenceValue == null) {
                return null;
            }
            var weekDays = [];
            var values = appUser.RecurrenceValue.split(';');
            angular.forEach(values, function (value, key) {
                if (value.indexOf('BYDAY') === 0) {
                    var valueParams = value.split('=');
                    if (valueParams.length > 1) {
                        weekDays = valueParams[1].split(',');
                    }
                }
            });
            if (weekDays.indexOf(weekDay) !== -1)
                return true;
            return null;
        };
        $scope.hotLineSettingsModel = {};

        $scope.checkedAllChanged = function () {
            $scope.MO = $scope.ALL;
            $scope.TU = $scope.ALL;
            $scope.WE = $scope.ALL;
            $scope.TH = $scope.ALL;
            $scope.FR = $scope.ALL;
            $scope.SA = $scope.ALL;
            $scope.SU = $scope.ALL;
        };

        $scope.openTimeToCall = function () {
            //$scope.ktpTimeToCall.open();
        };

        $scope.init = function (options) {
            $scope.appUser = appUser;
            $scope.event = {
                set: function () {
                }
            };

            var currentEvent;
            $timeout(function () {
                //var pane = $("#mypane").data("kendoMobilePane");
                //pane.navigate($("#editorForm").getKendoMobileView());

                //editEvent({ title: "test", start: new Date(), end: new Date() });
                //$("#recurrenceEditor").kendoMobileRecurrenceEditor({
                //    start: new Date(),
                //    //value: e.event.recurrenceRule,
                //    //timezone: "",
                //    pane: pane,
                //    //messages: self.scheduleConfig.messages.recurrenceEditor,
                //    change: function (ev) {
                //        //debugger;
                //        //e.event.set("recurrenceRule", this.value());
                //        console.log(this.value());
                //    }
                //});
                //var editorView = new kendo.View($("#editorForm").html());
                //var testView = $("#editHotlineDetails").getKendoMobileView().scrollerContent.append($("#editorForm").html());
                //var pane = kendo.mobile.ui.Pane.wrap(testView);
                //pane.wrapper.parent(".km-pane-wrapper").css("height", "500px");
                //pane.setOptions({
                //    viewShow: function (e) {
                //        //e.view.element.find(".k-button,.k-link").addClass("km-button km-widget km-primary").removeClass("k-button");
                //    }
                //});

                //angular.element("body").injector().invoke(["$compile", "$rootScope", function ($compile, $rootScope) {
                //    $compile(testView)($rootScope);
                //}]);
                ////editorView.element.find("ul[kendo-mobile-list-view]").kendoMobileListView();
                //var recEditor = $("#recurrenceEditor");            
                //var kMobileRecEditor = recEditor.kendoMobileRecurrenceEditor(
                //    {
                //        state: new Date(),
                //        pane: pane,
                //        value: "FREQ=WEEKLY;INTERVAL=2;BYDAY=WE,TH,FR,SA;WKST=SU",
                //        change: function () {
                //            console.log(this.value());
                //        }
                //    });
                //kMobileRecEditor.find(".k-button,.k-link").addClass("km-primary km-justified").removeClass("k-link");//kendoMobileButton();
            }, 0, true);

        };

        var editEvent = function (event) {
            var editor = $("#editor");
            var template = kendo.template($("#editor-template").html());

            editor.html(template({}));
            editor.find("#recurrenceEditor").kendoMobileRecurrenceEditor({
                start: new Date(),
                //value: e.event.recurrenceRule,
                //timezone: "",
                //messages: self.scheduleConfig.messages.recurrenceEditor,
                change: function (ev) {
                    //debugger;
                    //e.event.set("recurrenceRule", this.value());
                    console.log(this.value());
                }
            });
            kendo.bind(editor, event); //Bind the editor container (uses MVVM)

            editor.find("#save").click(function () {
            });

            editor.find("#cancel").click(function () {
            });
        }

        $scope.mockTestCall = function () {
            var firstNumber = true;
            if ($scope.mockTestCallIndex >= $scope.InputPairs.length) {
                $scope.mockTestCallData = 'Waiting for recording...';
                return;
            }
            $scope.mockTestCallData = 'Pressing ' + $scope.InputPairs[$scope.mockTestCallIndex].Numbers;
            if ($scope.InputPairs[$scope.mockTestCallIndex].Wait != '0')
                $scope.mockTestCallData += '... Waiting ' + $scope.InputPairs[$scope.mockTestCallIndex].Wait + ' sec';
            $timeout(function () { $scope.mockTestCallIndex++; $scope.mockTestCall(); }, (Number($scope.InputPairs[$scope.mockTestCallIndex].Wait)) * 1000, true);
        };

        $scope.makeTestCall = function () {
            if ($scope.PhoneNumber == null || $scope.PhoneNumber == ''
                || $scope.CallTimeSet == false
                || $scope.getNewRecurrenceValue() == null || $scope.getNewRecurrenceValue() == '') {
                return false;
            }
            $scope.saveDetails();
            $scope.mockTestCallIndex = 0;
            $scope.mockTestCallData = 'Calling +1 ' + $scope.PhoneNumber + '...';
            $timeout(function () { $scope.mockTestCall(); }, 15.5 * 1000, true);
            $timeout(function () {
                $("#making-test-call").kendoMobileModalView("open");
            }, 0, true);
            $scope.CallFinished = false;
            $scope.CallError = false;
            $scope.SettingsSaved = false;
            $scope.PlayFinished = false;
            //kMobileApp.showLoading();
            //make test call here
            plivoHttp.makeTestCall(appUser.Id, $scope.PhoneNumber + "", $scope.getFormattedDigits(), $scope.getMobileNumbersJoined("<"), 15, false).success(function (data) {
                $scope.call = {};
                if (data.error) {
                    $scope.call.error = data.error;
                    $scope.CallError = true;
                }
                else {
                    var player = $("#callRecordingPlayer");
                    $("#callRecordingPlayer").on("canplay", function () {
                        player[0].play();
                    });

                    $scope.TestCallLink = $sce.trustAsResourceUrl(data.recordingUrl);
                    player[0].load();
                }
                $scope.CallFinished = true;
                $scope.callUuid = data.callUuid;
                //kMobileApp.hideLoading();
            });
        };

        $scope.waitTranscibeCall = function () {
            $scope.hideMakeTestCallModal();
            if ($scope.MobileNumbers.length <= 1 && ($scope.MobileNumbers[0].value == null || $scope.MobileNumbers[0].value == '')) {
                $scope.goToMain();
                return;
            }
            $timeout(function () {
                $("#transcribing-call").kendoMobileModalView("open");
            }, 0, true);
            $scope.TranscribeFinished = false;
            $scope.TranscribeError = false;
            $scope.SettingsSaved = false;
            kMobileApp.showLoading();
            plivoHttp.getTranscription($scope.callUuid).success(function (data) {
                $scope.transcribe = {};
                if (data.error) {
                    $scope.transcribe.error = data.error;
                    $scope.TranscribeError = true;
                }
                else {
                    $scope.call.transcription = data.transcription;
                }
                $scope.TranscribeFinished = true;
                kMobileApp.hideLoading();
            });
        };

        $scope.hideMakeTestCallModal = function () {
            $timeout(function () {
                $("#making-test-call").kendoMobileModalView("close");
            }, 0, true);
        };

        $scope.hideTranscribingCallModal = function () {
            $timeout(function () {
                $("#transcribing-call").kendoMobileModalView("close");
            }, 0, true);
        };

        $scope.getMobileNumbersJoined = function (separator) {
            var mobileNumbers = [];
            angular.forEach($scope.MobileNumbers, function (item, index) {
                if (item.value != null)
                    mobileNumbers.push("1" + item.value);
            });
            return mobileNumbers.join(separator);
        };

        $scope.getNewRecurrenceValue = function () {
            var recurrenceValue = 'FREQ=WEEKLY;BYDAY=';
            if ($scope.MO)
                recurrenceValue += 'MO,';
            if ($scope.TU)
                recurrenceValue += 'TU,';
            if ($scope.WE)
                recurrenceValue += 'WE,';
            if ($scope.TH)
                recurrenceValue += 'TH,';
            if ($scope.FR)
                recurrenceValue += 'FR,';
            if ($scope.SA)
                recurrenceValue += 'SA,';
            if ($scope.SU)
                recurrenceValue += 'SU';
            recurrenceValue += ';WKST=SU';
            return recurrenceValue;
        };
        $scope.getNewAdditionalInput = function () {
            var additionalInput = '';
            angular.forEach($scope.InputPairs, function (item, index) {
                if (item.Numbers != '') {
                    additionalInput += item.Numbers;
                }
                additionalInput += ',';
                if (item.Wait != '') {
                    additionalInput += item.Wait;
                }
                additionalInput += ';';
            });
            return additionalInput;
        };

        $scope.saveDetails = function () {
            var user = {};
            user.HotLinePhoneNumber = $scope.PhoneNumber + "";
            user.HotLineAdditionalInput = $scope.getNewAdditionalInput();
            if ($scope.dtTimeToCall != null)
                user.CallTime = new Date($scope.dtTimeToCall.getTime() - $scope.dtTimeToCall.getTimezoneOffset() * 60 * 1000);
            user.EmailNotify = $scope.EmailNotify;
            //user.CallLimit = $scope.CallLimit;
            user.MobileNumbers = [];
            angular.forEach($scope.MobileNumbers, function (item, index) {
                if (item.value != null)
                    user.MobileNumbers.push(item.value + "");
            });
            user.RecurrenceValue = $scope.getNewRecurrenceValue();
            //kMobileApp.showLoading();
            userViewModelHttp.updateHotlineSettings($scope.appUser.Id, user)
                             .success(function (data) {
                                 $scope.appUser.HotLinePhoneNumber = user.HotLinePhoneNumber + "";
                                 $scope.appUser.HotLineAdditionalInput = user.HotLineAdditionalInput;

                                 $scope.appUser.CallTime = data.CallTime;

                                 $scope.appUser.EmailNotify = user.EmailNotify;
                                 $scope.appUser.MobileNumbers = user.MobileNumbers;
                                 $scope.appUser.RecurrenceValue = user.RecurrenceValue;
                                 //kMobileApp.hideLoading();
                                 $scope.SettingsSaved = true;
                                 //$scope.goToMain();
                             })
                             .error(function (data) {
                                 kMobileApp.hideLoading();
                                 // $scope.showMessage(data.error.innererror.message, "error");
                             });
        };

        $scope.goToMain = function () {
            $timeout(function () {
                kMobileApp.navigate("#!history");
            }, 0, true);
        };

        $scope.recEditorOptions = {
            //start: new Date(),
            //timezone: "Etc/UTC",
            //messages: "test message",

            size: { width: 1500, height: 300 },
            pane: $("#edit").data("kendoMobileView"),
            change: function () {
                //event.set("recurrenceRule", this.value());
            }
        };
    }]);