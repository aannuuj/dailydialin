﻿angular.module("ModuleHotLine").controller("loginCtrl", ["$scope", "$window", "userViewModelHttp",
    function ($scope, $window, userViewModelHttp) {
        var kMobileApp = kendo.mobile.application;

        $scope.onShow = function () {
        };

        $scope.init = function (options) {
            $scope.loginModel = {};
            $scope.loginModel.rememberMe = false;

            $scope.appUser = {};
            $scope.Email = '';
            $scope.Password = '';
            $scope.CPassword = '';
            $scope.appUser.profile = {};
            $scope.appUser.profile.FirstName = '';
            $scope.appUser.profile.LastName = '';
            $scope.appUser.profile.CountyId = '';
        };

        $scope.submitNormalForm = function () {
        	location.href = 'Mobile.html';
            /*$("#normalLogin").submit();
            if ($("#normalLogin").valid())
                kMobileApp.showLoading();*/
        };

        $scope.goToRegister = function () {
            kMobileApp.showLoading();
            location.href = 'Register';
        };
        $scope.getAccountType = function () {
            userViewModelHttp.getAccountType($scope.loginModel.email)
                             .success(function (data) {
                                 if (data.value && data.value!= "Local")
                                     $scope.submitExternalLogin(data.value);
                             });
                             
        };
        $scope.submitExternalLogin = function (providerName) {
            //kMobileApp.showLoading();
            $('#externalProvider').submit(function () {
                $(this).append('<input type="hidden" name="provider" value="' + providerName + '" />');
                return true;
            });
            $('#externalProvider').submit();
            if ($("#externalProvider").valid())
                kMobileApp.showLoading();
        };
    }]);