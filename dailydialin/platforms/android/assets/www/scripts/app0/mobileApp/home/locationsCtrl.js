﻿angular.module("ModuleHotLine").controller("locationsCtrl", ["$rootScope", "$scope", "$sce", "$timeout", "$window", "$location", "locationViewModelHttp", "appUser",
    function ($rootScope, $scope, $sce, $timeout, $window, $location, locationViewModelHttp, appUser) {
        var kMobileApp = kendo.mobile.application;
        $scope.locations = [];

        $scope.afterShow = function (e) {
        };

        $scope.init = function (options) {

        };
        $scope.onShow = function () {
            $scope.currentTime = new Date();
            $scope.getLocations();
            $scope.message = "";
        };

        $scope.formatDate = function (now) {
            var tzo = -now.getTimezoneOffset(),
            dif = tzo >= 0 ? '+' : '-',
            pad = function (num) {
                var norm = Math.abs(Math.floor(num));
                return (norm < 10 ? '0' : '') + norm;
            };
            return now.getFullYear()
                + '-' + pad(now.getMonth() + 1)
                + '-' + pad(now.getDate())
                + 'T' + pad(now.getHours())
                + ':' + pad(now.getMinutes())
                + ':' + pad(now.getSeconds())
                + dif + pad(tzo / 60)
                + ':' + pad(tzo % 60);
        };     
        $scope.getLocations = function () {            
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {                  
                    $scope.$apply(function () {
                        $scope.position = position;
                        kMobileApp.showLoading();
                        locationViewModelHttp.getLocations($scope.formatDate($scope.currentTime), $scope.position.coords.latitude, $scope.position.coords.longitude)
                        .success(function (data) {
                            if (!data.value || data.value.length == 0) {
                                $scope.message = "could not find any drug testing facility";
                            }
                            $scope.locations = data.value;
                            kMobileApp.hideLoading();
                        })
                        .error(function (data) {
                            kMobileApp.hideLoading();
                            $scope.message = "There was an error processing your request.";
                            //$scope.showMessage('There was an error processing your request.', 'error');
                        });
                    });                      
                }, function (e) {
                    $scope.$apply(function () {
                        $scope.message = e.message;
                    });
                }, { timeout: 3000 });

                
            }
        };

        $scope.showMessage = function (message, messageType) {
            $scope.notif.show(message, messageType);
        };

    }]);


