﻿angular.module("ModuleHotLine").controller("loginCtrl", ["$scope", "$window", "accountHttp",
    function ($scope, $window, accountHttp) {
        var kMobileApp = kendo.mobile.application;

        $scope.onShow = function () {
        };

        $scope.init = function (options) {
            $scope.loginModel = {};
            $scope.loginModel.rememberMe = false;

            $scope.appUser = {};
            $scope.Email = '';
            $scope.Password = '';
            $scope.CPassword = '';
            $scope.appUser.profile = {};
            $scope.appUser.profile.FirstName = '';
            $scope.appUser.profile.LastName = '';
            $scope.appUser.profile.CountyId = '';
        };

        $scope.submitNormalForm = function () {
            $("#normalLogin").submit();
            if ($("#normalLogin").valid())
                kMobileApp.showLoading();
        };

        $scope.submitLoginForm = function () {
            if ($scope.loginModel.email == null || $scope.loginModel.email == '' || $scope.loginModel.password == null || $scope.loginModel.password == '') {
                $scope.Validate = true;
                return;
            }
            kMobileApp.showLoading();
            accountHttp.login($scope.loginModel.email, $scope.loginModel.password, $scope.loginModel.rememberMe)
                       .success(function (data) {
                           kMobileApp.hideLoading();
                       }).error(function (data) {
                           $scope.serverErrors = [];
                           $scope.serverErrors.push(data.error_description);
                           kMobileApp.hideLoading();
                       });
        };

        $scope.goToRegister = function () {
            kMobileApp.showLoading();
            location.href = 'Register';
        };
        $scope.submitExternalLogin = function (providerName) {
            alert('here clck');
            //kMobileApp.showLoading();
            $('#externalProvider').submit(function () {
                $(this).append('<input type="hidden" name="provider" value="' + providerName + '" />');
                return true;
            });
            $('#externalProvider').submit();
            if ($("#externalProvider").valid())
                kMobileApp.showLoading();
        };
    }]);