﻿var moduleHotLine = angular.module("ModuleHotLine", ["kendo.directives", "ngCookies"]);
angular.module("ModuleHotLine").config(["$controllerProvider", "$provide", "$compileProvider", "$locationProvider",
function ($controllerProvider, $provide, $compileProvider, $locationProvider) {
    // Since the "shorthand" methods for component
    // definitions are no longer valid, we can just
    // override them to use the providers for post-
    // bootstrap loading.
    //$locationProvider.html5Mode({
    //    enabled: false,
    //    requireBase: false
    //});
    // $locationProvider.html5Mode({
    //        enabled: true,
    //        requireBase: false
    //}).hashPrefix('!');
    $provide.decorator('$browser', ['$delegate', function ($delegate) {
        $delegate.onUrlChange = function () { };
        $delegate.url = function () { return "" };
        return $delegate;
    }]);
    console.log("Config method executed.");
    console.log("s");
    // Let's keep the older references.
    var moduleHotLine = angular.module("ModuleHotLine");
    moduleHotLine._controller = moduleHotLine.controller;
    moduleHotLine._service = moduleHotLine.service;
    moduleHotLine._factory = moduleHotLine.factory;
    moduleHotLine._value = moduleHotLine.value;
    moduleHotLine._directive = moduleHotLine.directive;
    // Provider-based controller.
    moduleHotLine.controller = function (name, constructor) {
        $controllerProvider.register(name, constructor);
        return (this);
    };
    // Provider-based service.
    moduleHotLine.service = function (name, constructor) {
        $provide.service(name, constructor);
        return (this);
    };
    // Provider-based factory.
    moduleHotLine.factory = function (name, factory) {
        $provide.factory(name, factory);
        return (this);
    };
    // Provider-based value.
    moduleHotLine.value = function (name, value) {
        $provide.value(name, value);
        return (this);
    };
    // Provider-based directive.
    moduleHotLine.directive = function (name, factory) {
        $compileProvider.directive(name, factory);
        return (this);
    };
    // NOTE: You can do the same thing with the "filter"
    // and the "$filterProvider"; but, I don't really use
    // custom filters.                                           
}]).run(["$location", function ($location) {
    if (location.href.indexOf('/Account') > 0) {
        location.hash = "";
    }
}]);

angular.module("ModuleHotLine").controller("appCtrl", ["$rootScope", "$scope", "$timeout", "$cookies", function ($rootScope, $scope, $timeout, $cookies) {
    //alert('appCtrl');
    $scope.init = function (options) {
        //alert('init');
        console.log($scope);
        moduleHotLine.value("appConfigs", options);
        // //for jquery ajax calls   
        var authorizationHeaders = {
            headers: {
                "Authorization": "Bearer " + $cookies.get("accessTokenhotLineWebApi"),
                "Accept": "application/json;charset=utf-8"
            },
        };

        $.ajaxSetup(authorizationHeaders);
    };
    $scope.mobileAppInit = function () {
        //$timeout(function () {
        var kMobileApp = kendo.mobile.application;

        kMobileApp.pane.bind("navigate",
        function (e) {
            $rootScope.previousView = e.sender.view().id;
        });

        kMobileApp.changeView = function (tabStripObject, viewName, tabName) {
            kMobileApp.navigate(viewName);
            if (tabStripObject)
                tabStripObject.switchTo(tabName);
        };
        moduleHotLine.value("kMobileApp", kendo.mobile.application);
        //}, 0, true);
    };
}]);
angular.module("ModuleHotLine").factory('authHttpResponseInterceptor', ['$q', '$location', "$cookies", function ($q, $location, $cookies) {
    return {
        request: function (config) {
            // do something on success
            config.headers["Authorization"] = "Bearer " + $cookies.get("accessTokenhotLineWebApi");
            return config;
        },
        response: function (response) {
            return response || $q.when(response);
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                location.href = '/Account/LogOff';
            }
            return $q.reject(rejection);
        }
    }
}])
.config(['$httpProvider', function ($httpProvider) {
    //Http Intercpetor to check auth failures for xhr requests
    $httpProvider.interceptors.push('authHttpResponseInterceptor');

}]);