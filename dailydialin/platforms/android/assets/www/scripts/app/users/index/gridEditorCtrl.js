﻿angular.module("ModuleHotLine").controller("GridEditorCtrl", ["$scope", "$timeout", "$window", "timeZoneHttp" ,
    function ($scope,$timeout, $window, timeZoneHttp) {

        $scope.init = function () {
            $scope.dataItem.dirty = true;
            $scope.dataItem.RVALL = false; //RV Recurrence Value
            $scope.dataItem.RVMO = $scope.checkWeekDay('MO');
            $scope.dataItem.RVTU = $scope.checkWeekDay('TU');
            $scope.dataItem.RVWE = $scope.checkWeekDay('WE');
            $scope.dataItem.RVTH = $scope.checkWeekDay('TH');
            $scope.dataItem.RVFR = $scope.checkWeekDay('FR');
            $scope.dataItem.RVSA = $scope.checkWeekDay('SA');
            $scope.dataItem.RVSU = $scope.checkWeekDay('SU');
            $scope.dataItem.formattedAdditionalInput = $scope.getFormattedAdditionalInput($scope.dataItem.HotLineAdditionalInput);
            if ($scope.dataItem.RVMO &&
               $scope.dataItem.RVTU &&
               $scope.dataItem.RVWE &&
               $scope.dataItem.RVTH &&
               $scope.dataItem.RVFR &&
               $scope.dataItem.RVSA &&
               $scope.dataItem.RVSU)
                $scope.dataItem.RVALL = true;

            $scope.dataItem.MobileNumbersForm = [];
            angular.forEach($scope.dataItem.MobileNumbers, function (item, index) {
                $scope.mobileNumberAdded(Number(item.replace(/[\-() ]/g, "")));
            });
            if ($scope.dataItem.MobileNumbersForm.length == 0) {
                $scope.mobileNumberAdded(null);
            }
        };
        
		$scope.handleChange = function(selected, dataItem, data){
		};		
	
        
        $scope.checkedAllChanged = function () {
            $scope.dataItem.RVMO = $scope.dataItem.RVALL;
            $scope.dataItem.RVTU = $scope.dataItem.RVALL;
            $scope.dataItem.RVWE = $scope.dataItem.RVALL;
            $scope.dataItem.RVTH = $scope.dataItem.RVALL;
            $scope.dataItem.RVFR = $scope.dataItem.RVALL;
            $scope.dataItem.RVSA = $scope.dataItem.RVALL;
            $scope.dataItem.RVSU = $scope.dataItem.RVALL;
        };
        $scope.checkWeekDay = function (weekDay) {
            if ($scope.dataItem.RecurrenceValue == '' ||
                $scope.dataItem.RecurrenceValue == null) {
                return false;
            }
            var weekDays = [];
            var values = $scope.dataItem.RecurrenceValue.split(';');
            angular.forEach(values, function (value, key) {
                if (value.indexOf('BYDAY') === 0) {
                    var valueParams = value.split('=');
                    if (valueParams.length > 1) {
                        weekDays = valueParams[1].split(',');
                    }
                }
            });
            return weekDays.indexOf(weekDay) !== -1;
        };
       
         $scope.getFormattedAdditionalInput = function (hotlineAdditionalInput) {
            var formattedAdditionalInput = '';
            angular.forEach(hotlineAdditionalInput.split(';'), function (item, index) {
                var values = item.split(',');
                var numbers = values[0];
                var wait = values[1];
                if (numbers == null) {
                    numbers = '';
                }
                if (wait == null) {
                    wait = '';
                }
                if (wait != '') {
                    formattedAdditionalInput += numbers + ',' + wait + ' sec;';
                }
                else if (numbers != '') {
                    formattedAdditionalInput += numbers + ';';
                }
            });
            return formattedAdditionalInput;
        };

        $scope.dtCallTimePickerOpts = {
                                                    value: $scope.dataItem.dtCallTime,
                                                    change: function () {
                                                        $scope.dataItem.CallTime = this.value();
                                                        $scope.dataItem.dtCallTime = this.value();
                                                        
                                                    },
        };
        $scope.mobileNumberAdded = function (value) {
            $scope.dataItem.MobileNumbersForm.push({ value: value });
        };

        $scope.mobileNumberRemoved = function (index) {
            if ($scope.dataItem.MobileNumbersForm.length == 1)
                return;
            $scope.dataItem.MobileNumbersForm.splice(index, 1);
        };
        $scope.numberKeyDown = function (event) {
            if (event.keyCode == 189 ||
               event.keyCode == 107 ||
                event.keyCode == 109 ||
                event.keyCode == 190)
                event.preventDefault();
        };
        $scope.ddlTimeZoneOpts = {dataSource: {
                                                type: "odata-v4",        
                                                transport: {
                                                    read: function (options) {
                                                        var oDataOptions = kendo.data.transports.odata.parameterMap(options.data, "read");
                                                        timeZoneHttp.getAll()
                                                                                       .success(function (data) {
                                                                                           options.success(data);
                                                                                       });

                                                    },
                                                    parameterMap: function (options, operation) {                    
                                                        var paramMap = kendo.data.transports.odata.parameterMap(options);                    
                                                        delete paramMap.$format; // <-- remove format parameter                                                
                                                        return paramMap;
                                                    }
                                                },
                                                schema: {
                                                    total: function (data) { return data['@odata.count']; },
                                                    data: "value",
                                                    model: {
                                                        "id": "Id",
                                                        fields: {
                                                            Id: { type: "number" },
                                                            Name: {type: "string"},
                                                            UtcOffset: { type: "string" }

                                                        }
                                                    }
                                                },

                                            },
                                            dataValuePrimitive: true,
                                            optionLabel: "Select Timezone...",
                                            dataTextField: "Name",
                                            dataValueField: "Name",
         };

    }]);
	
	
	