﻿angular.module("ModuleHotLine").factory("timeZoneHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var timeZoneFactory = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata/TimeZoneViewModels";

    timeZoneFactory.getAll = function (params) {
        return $http.get(baseUrl, {params: params} );
    };

    return timeZoneFactory;
}]);