﻿angular.module("ModuleHotLine").factory("userFeedbackViewModelHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var userFeedbackViewModelFactory = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata/UserFeedbackViewModels";

    userFeedbackViewModelFactory.getAll = function (params) {
        return $http.get(baseUrl, {params: params} );
    };

    userFeedbackViewModelFactory.Create = function (userFeedbackViewModel) {
        return $http.post(baseUrl, userFeedbackViewModel);
    };

    return userFeedbackViewModelFactory;
}]);