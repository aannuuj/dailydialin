﻿angular.module("ModuleHotLine").factory("callLogViewModelHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var callLogViewModelFactory = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata/CallLogViewModels";

    callLogViewModelFactory.getAll = function (queryStringObject, content) {
        return $http.get(baseUrl,{params: queryStringObject});
    };
    return callLogViewModelFactory;
}]);