﻿angular.module("ModuleHotLine").factory("hotlineFormattingFactory", ["$http", "appConfigs", function ($http, appConfigs) {
    var hotlineFormattingFactory = {};    

    hotlineFormattingFactory.getWeekDaysString = function (recurrenceValue) {
            if (recurrenceValue == null) {
                return '';
            }
            var weekDaysString = '';
            var values = recurrenceValue.split(';');
            angular.forEach(values, function (value, key) {
                if (value.indexOf('BYDAY') === 0) {
                    var valueParams = value.split('=');
                    if (valueParams.length > 1) {
                        weekDaysString = valueParams[1];
                    }
                }
            });
            return weekDaysString;
    };
    hotlineFormattingFactory.getDtCallTime = function (callTime) {
        if (callTime != null) {
            var dtCallTime = new Date(callTime);
            return new Date(dtCallTime.getTime() + dtCallTime.getTimezoneOffset() * 60 * 1000);

        }
        else
            return '';
    };
    hotlineFormattingFactory.getFormattedCallTime = function (callTime) {
            if (callTime != null) {
                var dtCallTime = new Date(callTime);
                return kendo.toString(new Date(dtCallTime.getTime() + dtCallTime.getTimezoneOffset() * 60 * 1000), 'hh:mm tt');

            }
            else
                return '';
    };

    return hotlineFormattingFactory;
}]);