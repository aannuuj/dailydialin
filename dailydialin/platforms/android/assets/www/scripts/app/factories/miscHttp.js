﻿angular.module("ModuleHotLine").factory("miscHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var miscHttp = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata";

    miscHttp.addShareDays = function (shared_on, user_id) {
        return $http.post(baseUrl + "/AddLikeDaysToSubscription", { shared_on: shared_on, user_id: user_id });
    };

    return miscHttp;
}]);