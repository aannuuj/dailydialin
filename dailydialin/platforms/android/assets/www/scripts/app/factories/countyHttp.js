﻿angular.module("ModuleHotLine").factory("countyHttp", ["$http", "appConfigs", function ($http, appConfigs) {
    var countyFactory = {};
    var baseUrl = appConfigs.hotLineWebApiUrl + "/odata/CountyViewModels";

    countyFactory.getAll = function (params) {
        return $http.get(baseUrl, {params: params} );
    };

    return countyFactory;
}]);