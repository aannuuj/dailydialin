﻿angular.module("ModuleHotLine").controller("detailsCtrl", ["$scope", "$window", "$timeout", "appUser", "userViewModelHttp", "timeZoneHttp",
    function ($scope, $window, $timeout, appUser, userViewModelHttp, timeZoneHttp) {
        var kMobileApp = kendo.mobile.application;
        var getTimeZoneByName = function (timeZoneName) {
            var matchedTimeZones = $($scope.timeZones).filter(function (index, item) {
                if (item.Name == timeZoneName)
                    return item;
            });
            return matchedTimeZones[0];
        };

        $scope.onShow = function () {
            $scope.FirstName = appUser.FirstName;
            $scope.LastName = appUser.LastName;
            $scope.CountyId = appUser.CountyId;
            //$scope.TimeZoneUtcOffset = appUser.TimeZoneUtcOffset;
            $scope.TimeZoneName = appUser.TimeZoneName;
            $scope.EmailAddress = appUser.EmailAddress;
            $scope.PromoCode = appUser.PromoCode;
            $scope.NewPassword = '';
            $scope.CNewPassword = '';
            $scope.OldPassword = '';
        };

        $scope.init = function (options) {
            kMobileApp.showLoading();            
            timeZoneHttp.getAll({ "$orderby": "Name" }).success(function (data) {
                $scope.timeZones = data.value;
                $scope.TimeZoneUtcOffset = getTimeZoneByName(appUser.TimeZoneName);

                kMobileApp.hideLoading();
            });
            $scope.appUser = appUser;
        };
        $scope.saveDetails = function () {
            var user = {};
            user.FirstName = $scope.FirstName;
            user.LastName = $scope.LastName;
            user.EmailAddress = $scope.EmailAddress;
            user.CountyId = $scope.CountyId;
            user.TimeZoneName = $scope.TimeZoneUtcOffset.Name;
            user.PromoCode = $scope.PromoCode;
            user.IsRecurringPayment = appUser.IsRecurringPayment;
            kMobileApp.showLoading();
            userViewModelHttp.updateProfile($scope.appUser.Id, user)
                             .success(function (data) {
                                 kMobileApp.hideLoading();
                                 if ($scope.appUser.EmailAddress.toLowerCase() != user.EmailAddress.toLowerCase()) {
                                     location.href = '/Account/LogOff';
                                 }
                                 $scope.appUser.FirstName = data.FirstName;
                                 $scope.appUser.LastName = data.LastName;
                                 $scope.appUser.EmailAddress = data.EmailAddress;
                                 $scope.appUser.CountyId = data.CountyId;
                                 $scope.appUser.TimeZoneName = data.TimeZoneName;
                                 $scope.appUser.CountyName = data.CountyName;
                                 $scope.appUser.StateName = data.StateName;
                                 $scope.appUser.PromoCode = data.PromoCode;
                                 $scope.appUser.IsRecurringPayment = data.IsRecurringPayment;
                                 $scope.PromoCode = data.PromoCode;
                                 $scope.goToMain();
                             })
                             .error(function (data) {
                                 kMobileApp.hideLoading();
                                 $scope.showMessage(data.error.innererror.message, "error");
                             });
        };
        $scope.changePassword = function () {
            if ($scope.OldPassword == "" || $scope.NewPassword == "" || $scope.CNewPassword == "") {
                $scope.showMessage("All password fields must be filled.", "error");
            }
            else if ($scope.NewPassword != $scope.CNewPassword) {
                $scope.showMessage("Password and Confirm Password must match.", "error");
            }
            else {
                kMobileApp.showLoading();
                userViewModelHttp.changePassword($scope.appUser.Id, $scope.OldPassword, $scope.NewPassword)
                                 .success(function () {
                                     kMobileApp.hideLoading();
                                 })
                                 .error(function (data) {
                                     kMobileApp.hideLoading();
                                     $scope.showMessage(data.error.innererror.message, "error");
                                 });

                //Change password here
            }
        };
        $scope.goToMain = function () {
            $timeout(function () {
                kMobileApp.navigate("#!history");
            }, 0, true);
        };
        $scope.showMessage = function (message, messageType) {
            $scope.notif.show(message, messageType);
            //alert(message);
        };
    }]);