﻿angular.module("ModuleHotLine").controller("indexCtrl", ["$scope", "$sce", "$window", "appUser",
    function ($scope, $sce, $window, appUser) {
        var kMobileApp = kendo.mobile.application;
        $scope.onShow = function () {
        };

        $scope.init = function (options) {
            $scope.appUser = appUser;
            
        };

        $scope.onShow = function () {
            
        };
        
        $scope.getFormattedCallTime = function () {
            if (appUser.CallTime != null) {
                var dtCallTime = new Date(appUser.CallTime);
                return kendo.toString(new Date(dtCallTime.getTime() + dtCallTime.getTimezoneOffset() * 60 * 1000), 'hh:mm tt');

            }
            else
                return '';
        };

        $scope.getFormattedAdditionalInput = function () {
            var formattedAdditionalInput = '';
            angular.forEach(appUser.HotLineAdditionalInput.split(';'), function (item, index) {
                var values = item.split(',');
                var numbers = values[0];
                var wait = values[1];
                if (numbers == null) {
                    numbers = '';
                }
                if (wait == null) {
                    wait = '';
                }
                if (wait != '') {
                    formattedAdditionalInput += numbers + ',' + wait + ' sec;';
                }
                else if (numbers != '') {
                    formattedAdditionalInput += numbers + ';';
                }
            });
            return formattedAdditionalInput;
        };

        $scope.getWeekDaysString = function () {
            if (appUser.RecurrenceValue == null) {
                return '';
            }
            var weekDaysString = '';
            var values = appUser.RecurrenceValue.split(';');
            angular.forEach(values, function (value, key) {
                if (value.indexOf('BYDAY') === 0) {
                    var valueParams = value.split('=');
                    if (valueParams.length > 1) {
                        weekDaysString = valueParams[1];
                    }
                }
            });
            return weekDaysString;
        };

        $scope.changeDetails = function () {

        };
        $scope.changeEmail = function () {

        };
        $scope.changePassword = function () {

        };
        $scope.changeHotLineSettings = function () {

        };
    }]);