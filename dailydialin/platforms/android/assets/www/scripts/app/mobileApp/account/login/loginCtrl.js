﻿angular.module("ModuleHotLine").controller("loginCtrl", ["$scope", "$window", "userViewModelHttp","accountHttp",
    function ($scope, $window, userViewModelHttp, accountHttp) {
        console.log('loginCtrl');
        var kMobileApp = kendo.mobile.application;

        $scope.onShow = function () {
        };

        $scope.init = function (options) {
            console.log('loginCtrl-:init');
            $scope.loginModel = {};
            $scope.loginModel.rememberMe = false;

            $scope.appUser = {};
            $scope.Email = '';
            $scope.Password = '';
            $scope.CPassword = '';
            $scope.appUser.profile = {};
            $scope.appUser.profile.FirstName = '';
            $scope.appUser.profile.LastName = '';
            $scope.appUser.profile.CountyId = '';
        };

        $scope.submitNormalForm = function () {
            var email = document.getElementById('Email').value;
            var Password = document.getElementById('Password').value;
            console.log('Login Here:' + email + '\n' + Password);
            //console.log(JSON.stringify(accountHttp.login(email, Password,"true")));
            accountHttp.login(email, Password, "true").success(function (res) {
                console.log('success');
                console.log(JSON.stringify(res));
                console.log(res);
                alert('You have login successfully.Please wait to open home page');
                location.href = 'Mobile.html';
            }).error(function (data, status) {
                console.error('Repos error', status, data);
                alert(data.error_description);
            });
          /*  $.ajax({
                type: "POST",
                url: 'https://dailydialin.com:700/token',
                crossDomain:true,
                dataType: "json",// "application/json;charset=UTF-8",
                contentType: "application/x-www-form-urlencoded",
                data: "grant_type=password&username=" + email + "&password=" + Password,
                async: true,
                cache: false,
                success: function (result) {
                    console.log(result);
                    alert(result);
                },
                error: function (data) {
                    console.log(data);
                }
            });
            $.ajax({
                url: "https://dailydialin.com:700/token",
                dataType: "jsonp",
                jsonp: "mycallback",
                success: function (data) {
                    alert("Name:" + data.name + "nage:" + data.age + "nlocation:" + data.location);
                }
            });
            */
            return;
            $("#normalLogin").submit();
            if ($("#normalLogin").valid())
                kMobileApp.showLoading();
        };

        $scope.goToRegister = function () {
            kMobileApp.showLoading();
            location.href = 'Register';
        };
        $scope.getAccountType = function () {
            userViewModelHttp.getAccountType($scope.loginModel.email)
                             .success(function (data) {
                                 if (data.value && data.value!= "Local")
                                     $scope.submitExternalLogin(data.value);
                             });
                             
        };
        $scope.submitExternalLogin = function (providerName) {
            alert(123);
            //kMobileApp.showLoading();
            $('#externalProvider').submit(function () {
                $(this).append('<input type="hidden" name="provider" value="' + providerName + '" />');
                return true;
            });
            $('#externalProvider').submit();
            if ($("#externalProvider").valid())
                kMobileApp.showLoading();
        };
    }]);