﻿angular.module("ModuleHotLine").controller("registerCtrl", ["$scope", "$window", "accountHttp", "timeZoneHttp",
    function ($scope, $window, accountHttp, timeZoneHttp) {
        var kMobileApp = kendo.mobile.application;

        $scope.onShow = function () {
        };

        $scope.init = function (options) {
            kMobileApp.showLoading();
            timeZoneHttp.getAll({ "$orderby": "Name" }).success(function (data) {
                $scope.timeZones = data.value;
                kMobileApp.hideLoading();
            });
        };

        $scope.register = function () {
            if ($scope.TimeZoneUtcOffset == null || $scope.Email == null || $scope.Email == '' || $scope.Password == null || $scope.Password == '' || $scope.ConfirmPassword != $scope.Password) {
                $scope.Validate = true;
                return;
            }

            var user = {
                FirstName: $scope.FirstName,
                LastName: $scope.LastName,
                TimeZoneUtcOffset: $scope.TimeZoneUtcOffset.UtcOffset,
                TimeZoneName: $scope.TimeZoneUtcOffset.Name,
                Email: $scope.Email,
                Password: $scope.Password,
                ConfirmPassword: $scope.ConfirmPassword,
                Referral: $scope.Referral
            };
            kMobileApp.showLoading();
            accountHttp.register(user).success(function (data) {
                //do something here
                kMobileApp.hideLoading();
            }).error(function (data) {
                $scope.serverErrors = [];
                angular.forEach(data.ModelState[""], function (item, index) {
                    $scope.serverErrors.push(item);
                });
                kMobileApp.hideLoading();
            });
        };
    }]);