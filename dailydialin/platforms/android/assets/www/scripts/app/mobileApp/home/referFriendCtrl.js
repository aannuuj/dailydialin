﻿angular.module("ModuleHotLine").controller("referFriendCtrl", ["$scope", "$window", "miscHttp", "userViewModelHttp", "appUser", "appConfigs",
     function ($scope, $window, miscHttp, userViewModelHttp, appUser, appConfigs) {
         var kMobileApp = kendo.mobile.application;
         $scope.init = function () {
             $scope.appUser = appUser;
         };
         $scope.onShow = function () {
             $scope.PromoCode = appUser.PromoCode;
         };
         $scope.closeModalView = function () {
             $('\#share-social-link').kendoMobileModalView('close');
         };
         $scope.promoCodeKeyDown = function (event) {
             console.log(event.keyCode);
             if ((event.keyCode >= 48 && event.keyCode <= 57 && event.shiftKey == false) || // 0-9 alphanumeric keypad
                 (event.keyCode >= 65 && event.keyCode <= 90) ||// a-zA-Z
                 (event.keyCode >= 96 && event.keyCode <= 105) ||// 0-9 numeric keypad
                 (event.keyCode >= 37 && event.keyCode <= 40) ||// arrow keys
                 (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 9 || event.keyCode == 110))
                 return;
             event.preventDefault();
         };
         $scope.savePromoCode = function () {
             if (!$scope.PromoCode) {
                 $scope.showMessage("Promo Code is required", "error");
                 return;
             }

             var user = {
                 FirstName: appUser.FirstName,
                 LastName: appUser.LastName,
                 EmailAddress: appUser.EmailAddress,
                 CountyId: appUser.CountyId,
                 TimeZoneName: appUser.TimeZoneName,
                 PromoCode: $scope.PromoCode.toUpperCase(),
                 IsRecurringPayment: appUser.IsRecurringPayment,
             };

             kMobileApp.showLoading();
             userViewModelHttp.updateProfile($scope.appUser.Id, user)
                              .success(function (data) {
                                  kMobileApp.hideLoading();

                                  $scope.appUser.FirstName = data.FirstName;
                                  $scope.appUser.LastName = data.LastName;
                                  $scope.appUser.EmailAddress = data.EmailAddress;
                                  $scope.appUser.CountyId = data.CountyId;
                                  $scope.appUser.TimeZoneName = data.TimeZoneName;
                                  $scope.appUser.CountyName = data.CountyName;
                                  $scope.appUser.StateName = data.StateName;
                                  $scope.appUser.PromoCode = data.PromoCode;
                                  $scope.appUser.IsRecurringPayment = data.IsRecurringPayment;
                                  $scope.PromoCode = data.PromoCode;
                                  $scope.showMessage("Promo code changed successfully", "info");
                              })
                              .error(function (data) {
                                  kMobileApp.hideLoading();
                                  $scope.showMessage(data.error.innererror.message, "error");
                              });
         };
         $scope.showMessage = function (message, messageType) {
             $scope.notif.show(message, messageType);
             //alert(message);
         };
         $scope.shareOnFacebook = function () {
             if (!$scope.PromoCode) {
                 $scope.showMessage("Promo Code is required", "error");
                 return;
             }

             FB.ui({
                 href: 'http://dailydialin.com/Account/Register?RefCode=' + appUser.PromoCode,
                 picture: 'https://dailydialin.com/Content/images/Dailydialin-share.png',
                 method: 'share',
                 title: 'Dialy Dial-In',  // The same than name in feed method
             }, function (response) {
                 if (!response) {
                     return;
                 }

                 kMobileApp.showLoading();
                 miscHttp.addShareDays("facebook", appUser.Id)
                     .success(function (data) {
                         $scope.updateAppUser(data);
                         kMobileApp.hideLoading();
                     }).error(function (data) {
                         kMobileApp.hideLoading();
                         $scope.showMessage(data.error.message, "error");
                     });;
             });
         };

         $scope.updateAppUser = function (data) {
             appUser.RemainingFreeDays = data.RemainingFreeDays;
             appUser.SharedOnFacebook = data.SharedOnFacebook;
             appUser.SharedOnGoogle = data.SharedOnGoogle;
             appUser.SharedOnTwitter = data.SharedOnTwitter;
         };
         $scope.shareOnGooglePlus = function () {
             if (!$scope.PromoCode) {
                 $scope.showMessage("Promo Code is required", "error");
                 return;
             }

             $window.open('https://plus.google.com/share?url=' + encodeURI("http://dailydialin.com/Account/Register?RefCode=" + appUser.PromoCode));

             kMobileApp.showLoading();
             miscHttp.addShareDays("google", appUser.Id)
                     .success(function (data) {
                         $scope.updateAppUser(data);
                         kMobileApp.hideLoading();
                     }).error(function (data) {
                         kMobileApp.hideLoading();
                         $scope.showMessage(data.error.message, "error");
                     });
         };
     }]);