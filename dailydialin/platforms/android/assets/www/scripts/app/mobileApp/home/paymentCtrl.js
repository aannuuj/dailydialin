﻿function updatePaymentProfileStatus(hasPaymentProfile) {
    var $scope = angular.element('#creditDetails').scope();
    $scope.updatePaymentProfileStatus(hasPaymentProfile);
    $scope.$apply(); //tell angular to check dirty bindings again
}
angular.module("ModuleHotLine").controller("paymentCtrl", ["$scope", "$window", "$timeout", "appUser", "paymentViewModelHttp", "userViewModelHttp", "appConfigs",
    function ($scope, $window, $timeout, appUser, paymentViewModelHttp, userViewModelHttp, appConfigs) {
        var kMobileApp = kendo.mobile.application;

        $scope.init = function (options) {
            
        };

        $scope.onShow = function () {
            $scope.hasPaymentProfile = appUser.HasPaymentProfile;
            $scope.RemainingDays = appUser.RemainingFreeDays;
            $scope.IsRecurringPayment = appUser.IsRecurringPayment;
            $scope.appUser = appUser;
        };

        $scope.isRecurringPaymentChanged = function () {
            var user = {
                FirstName: appUser.FirstName,
                LastName: appUser.LastName,
                EmailAddress: appUser.EmailAddress,
                CountyId: appUser.CountyId,
                TimeZoneName: appUser.TimeZoneName,
                PromoCode: appUser.PromoCode,
                IsRecurringPayment: $scope.IsRecurringPayment
            };
            kMobileApp.showLoading();
            userViewModelHttp.updateProfile($scope.appUser.Id, user)
                             .success(function (data) {
                                 kMobileApp.hideLoading();

                                 $scope.appUser.FirstName = data.FirstName;
                                 $scope.appUser.LastName = data.LastName;
                                 $scope.appUser.EmailAddress = data.EmailAddress;
                                 $scope.appUser.CountyId = data.CountyId;
                                 $scope.appUser.TimeZoneName = data.TimeZoneName;
                                 $scope.appUser.CountyName = data.CountyName;
                                 $scope.appUser.StateName = data.StateName;
                                 $scope.appUser.PromoCode = data.PromoCode;
                                 $scope.appUser.IsRecurringPayment = data.IsRecurringPayment;
                                 $scope.IsRecurringPayment = data.IsRecurringPayment;
                                 if (!$scope.IsRecurringPayment) {
                                     var message = "Your subscription will not be auto-renewed once it runs out of days. We recommend keeping this option on.";
                                     $scope.showMessageBoxModal("Error", message);
                                 }
                             })
                             .error(function (data) {
                                 kMobileApp.hideLoading();
                                 $scope.showMessageBoxModal("Error", data.error.innererror.message);
                             });
        };

        $scope.editPaymentInfo = function () {
            //kMobileApp.showLoading();
            window.open(appConfigs.hotLineWebUrl + '/Home/PaymentDetails/' + appUser.Id, '_blank');
        };

        $scope.makePayment = function () {
            kMobileApp.showLoading();
            paymentViewModelHttp.makePayment({ Id: appUser.Id, SubscriptionType: 'MonthlySub' })
            .success(function (data) {
                $scope.toggleVerifyPaymentModal(false);
                $scope.RemainingDays = data.RemainingDays;
                appUser.RemainingFreeDays = data.RemainingDays;
                $scope.appUser.PaidAmount += data.Amount;
                appUser.PaidAmount += data.Amount;
                var message = data.AddedSubDays + ' days have been added to your subscription. The charges are $' + data.Amount + '.';
                $scope.showMessageBoxModal("Info", message);
                kMobileApp.hideLoading();
            }).error(function (data) {
                $scope.toggleVerifyPaymentModal(false);
                var message = data.error.innererror.message;
                $scope.showMessageBoxModal("Error", message);
                kMobileApp.hideLoading();
            });
        };

        $scope.updatePaymentProfileStatus = function (hasPaymentProfile) {
            $scope.hasPaymentProfile = hasPaymentProfile;
            appUser.HasPaymentProfile = hasPaymentProfile;
            //kMobileApp.hideLoading();
        };

        $scope.showMessageBoxModal = function (messageType, messageContent) {
            $scope.messageType = messageType;
            $scope.messageContent = messageContent;
            $timeout(function () {
                $("#message-box").kendoMobileModalView("open");
            }, 0, true);
        };

        $scope.hideMessageBoxModal = function () {
            $timeout(function () {
                $("#message-box").kendoMobileModalView("close");
            }, 0, true);
        };

        $scope.toggleVerifyPaymentModal = function (show) {
            $timeout(function () {
                $("#verify-payment").kendoMobileModalView(show ? "open" : "close");
            }, 0, true);
        };

    }]);