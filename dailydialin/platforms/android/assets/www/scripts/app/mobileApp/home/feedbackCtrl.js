﻿angular.module("ModuleHotLine").controller("feedbackCtrl", ["$rootScope", "$scope", "$timeout", "$window", "userFeedbackViewModelHttp", "appUser",
    function ($rootScope, $scope, $timeout, $window, userFeedbackViewModelHttp, appUser) {
        var kMobileApp = kendo.mobile.application;
        $scope.init = function (options) {
            $scope.appUser = appUser;
        };

        $scope.onShow = function () {
            $scope.Email = appUser.EmailAddress;
            $scope.PageUrl = $rootScope.previousView;
        };

        $scope.saveFeedback = function () {
            kMobileApp.showLoading();
            var feedback = {
                UserId: appUser.Id,
                PageUrl: $scope.PageUrl,
                Description: $scope.Description,
                CreatedDate: new Date()
            };
            userFeedbackViewModelHttp.Create(feedback).success(function (data) {
                kMobileApp.hideLoading();
                $scope.showMessage("Thank you for your feedback.", "info");
                $scope.PageUrl = '';
                $scope.Description = '';
            });
        };

        $scope.showMessage = function (message, messageType) {
            $scope.notif.show(message, messageType);
            //alert(message);
        };
    }]);