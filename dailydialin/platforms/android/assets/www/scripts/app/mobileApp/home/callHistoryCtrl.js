﻿angular.module("ModuleHotLine").controller("callHistoryCtrl", ["$rootScope", "$scope", "$sce",  "$timeout", "$window", "$location", "userViewModelHttp", "appUser", 
    function ($rootScope, $scope, $sce, $timeout, $window, $location, userViewModelHttp, appUser) {        
        var kMobileApp = kendo.mobile.application;
        var callLogId = undefined;
        var $filter = undefined;
        var parseLocation = function (location) {
            var pairs = location.substring(1).split("&");
            var obj = {};
            var pair;
            var i;

            for (i in pairs) {
                if (pairs[i] === "") continue;

                pair = pairs[i].split("=");
                obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
            }

            return obj;
        };
        $scope.loadMoreCallLogViewModels = function () {
            if ($scope.callLogViewModels.length >= $scope.totalCallLogViewModels &&
                $scope.totalCallLogViewModels != -1)
                return;
            kMobileApp.showLoading();
            
            if (callLogId) {
                $filter = "Id eq " + callLogId;
                callLogId = null;
            }
            else {
                $filter = null;
            }
            userViewModelHttp.getCallLogViewModels(appUser.Id, { $top: 5, $skip: $scope.callLogViewModels.length, $filter: $filter,  $count: true, $format: "json", $orderby: "Timestamp desc" })
                            .success(function (data) {
                                $scope.totalCallLogViewModels = data["@odata.count"];
                                $.each(data.value, function (idx, item) {                                    
                                    item.TrustedSource = $sce.trustAsResourceUrl(item.Source);
                                    item.dtTimestamp = new Date(item.Timestamp);//kendo.parseDate(item.Timestamp, "yyyy-MM-ddTHH:mm:ss");
                                    item.formattedTimestamp = kendo.toString(item.dtTimestamp, "MMM dd, hh:mm tt")
                                    if(item.Source.lastIndexOf("mp3") == item.Source.length -3)
                                        item.Type = "audio/mpeg";
                                    else if (item.Source.lastIndexOf("wav") == item.Source.length - 3)
                                        item.Type = "audio/wav";
                                });

                                $scope.callLogViewModels = $scope.callLogViewModels.concat(data.value);
                                kMobileApp.hideLoading();
                            });
        };
        $scope.afterShow = function(e){            
        };
	
        $scope.init = function (options) {
            callLogId = parseLocation(window.location.search).id;
            console.log($location.search().id);
            console.log(options);
            $rootScope.model = options;
            //$scope.callLogViewModels = [];
            //$scope.loadMoreCallLogViewModels();

        };
        $scope.onShow = function () {
            $scope.totalCallLogViewModels = -1;
            $scope.callLogViewModels = [];
            $scope.loadMoreCallLogViewModels();
        };
    }]);
	
	
	