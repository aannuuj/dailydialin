﻿angular.module("ModuleHotLine").filter('millitaryTimeToStandardTime', function() {
  return function(input) {
      var inputTimeInString = "" + input;
      if (inputTimeInString.length < 4) {
          inputTimeInString = "0000" + inputTimeInString;
          inputTimeInString = inputTimeInString.slice(inputTimeInString.length - 4);
      }

      var hours24 = parseInt(inputTimeInString.substring(0, 2), 10);
            var hours = ((hours24 + 11) % 12) + 1;
            var amPm = hours24 > 11 ? 'PM' : 'AM';
            var minutes = inputTimeInString.substring(2);

            return hours + ':' + minutes + " " + amPm;
  };
})